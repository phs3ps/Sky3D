Sky3d is a nuclear time-dependent Hartree-Fock code.

It represents wave functions in a symmetry-unrestricted three-dimensional cartesian box, and operates in static and time-dependent modes.  The static mode is mainly for the generation of initial conditions for time-depdendent calcualtions, but the static version can be used in its own right to calculate ground states, or other states of interest via suitable constraints.

The built-in nuclear interaction is the Skymre effective interaction + Coulomb.  The code has been published as versions 1.0 and 1.1 in Computer Physics Communications: [v1.0](http://dx.doi.org/10.1016/j.cpc.2014.04.008), [v1.1](https://doi.org/10.1016/j.cpc.2018.03.012).  The git repository has some branches containing further developments beyond these published versions, such as a density constraint, and periodic cells for neutron star matter.

To communicate with the authors, please email p.stevenson@surrey.ac.uk
